## CHOOOSE Frontend Task
[Live Demo](https://cdeegan-interview-challenges.gitlab.io/chooose-frontend-task/)

### Usage
1. Clone the application and navigate to the installed directory
2. Install the application with `npm install`
3. Run `npm run db` to launch the sample database
4. Run `npm start` to launch the React dev server
5. Go to [http://localhost:3000](http://localhost:3000) to see the app
6. (optional) Modify the contents of `api/db.json` and refresh the browser page to see changes
7. (optional) Run `npm test` to run the test suite

### Room for improvement
- optimised card images + lazy load
- card pagination, sort, filter
- redux for more complex state management later on
- linting + style enforcement
- improved project structure as file count grows
- strip data-testid from prod bundle
- api root as environment var