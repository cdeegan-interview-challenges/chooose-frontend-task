import Itinerary from '../interfaces/Itinerary';

const API_ROOT = 'http://localhost:3030';

export const SAMPLE_ITINERARIES: Itinerary[] = [
  { id: 0, title: 'European Quest', imgSrc: '//media.cntraveler.com/photos/5ad7a505e0fff73b656ee2cf/master/w_1920%2Cc_limit/GettyImages-825083444.jpg', countryCount: 28, dayDuration: 8, emissionKgs: 2150, rating: 4.5 },
  { id: 1, title: 'Autumn Roadtrip', imgSrc: '//media.cntraveler.com/photos/5ad79c2dee4f286b51bedad8/master/w_1920%2Cc_limit/Chott-el-Jerid-GettyImages-135165198.jpg', countryCount: 3, dayDuration: 14, emissionKgs: 1450, rating: 4.2 },
  { id: 2, title: 'Diving adventure in Egypt', imgSrc: '//media.cntraveler.com/photos/59960c6230e0b978de5929e9/master/w_1920%2Cc_limit/Jellyfish-Lake-palau-GettyImages-150629831.jpg', countryCount: 1, dayDuration: 2, emissionKgs: 750, rating: 3.7 },
  { id: 3, title: 'Italian Adventure', imgSrc: '//media.cntraveler.com/photos/5ad79a121c56371a0c88d848/master/w_1920%2Cc_limit/Yuanyang-GettyImages-146577864.jpg', countryCount: 1, dayDuration: 5, emissionKgs: 1050, rating: 5 },
  { id: 4, title: 'Colorado Mountaineering', imgSrc: '//media.cntraveler.com/photos/5ad79dd2a4034c61525d8a2f/master/w_1920%2Cc_limit/Antelope-Canyon-GettyImages-162813219.jpg', countryCount: 1, dayDuration: 1, emissionKgs: 250, rating: 4.3 },
  { id: 5, title: 'South East Asian Experience', imgSrc: '//media.cntraveler.com/photos/5ad79fc6fa45355065f2047c/master/w_1920%2Cc_limit/Moraine-Lake%2C-Banff-National-Park-GettyImages-517747003.jpg', countryCount: 5, dayDuration: 21, emissionKgs: 2000, rating: 4.6 }
];

export const getAllItineraries = async (): Promise<Itinerary[]> => {
  // for production (Gitlab pages), just return the sample list with simulated delay
  if (process.env.NODE_ENV === 'production') {
    return new Promise((resolve) => {
      setTimeout(() => {
        return resolve(SAMPLE_ITINERARIES);
      }, 2500);
    });
  }
  
  const response = await fetch(`${API_ROOT}/itineraries`);
  return response.json();
};