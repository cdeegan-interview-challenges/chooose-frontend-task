export const formatEmissionsWithUnit = (emissionKgs: number) => {
    if (emissionKgs < 1000) { return `${emissionKgs} kg`; }
    if (emissionKgs % 1000 === 0) { return `${(emissionKgs / 1000)} t`; }
    return `${(emissionKgs / 1000).toFixed(2)} t`
};
