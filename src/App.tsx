import { useEffect, useState } from 'react';
import { Alert, AlertIcon, Center, ChakraProvider, SimpleGrid, Spinner } from '@chakra-ui/react';
import { getAllItineraries } from './utils/api';
import { ItineraryCard } from './components/ItineraryCard';
import Itinerary from './interfaces/Itinerary';
import theme from './theme';

export const App = () => {
  const [error, setError] = useState(null);
  const [isLoading, setLoading] = useState<boolean>(true);
  const [itineraries, setItineraries] = useState<Itinerary[]>([]);

  useEffect(() => {
    getAllItineraries().then((itineraryResponse) => {
      setItineraries(itineraryResponse);
    }).catch((err) => {
      setError(err);
    }).finally(() => {
      setLoading(false);
    });
  }, []);
  
  return <ChakraProvider theme={theme}>
    {
      isLoading &&
      <Center p={4} data-testid='loading-spinner'><Spinner /></Center>
    }
    {
      !isLoading && error &&
      <Alert status='error'>
        <AlertIcon />
        An error has occurred
      </Alert>
    }
    {
      !isLoading && !error && itineraries.length === 0 && 
      <Alert status='info'>
        <AlertIcon />
        No data to display
      </Alert>
    }
    {
      !isLoading && !error && itineraries.length > 0 &&
      <SimpleGrid
      data-testid='itinerary-grid'
      columns={[1, 1, 2, 3, 4]}
      spacing={4}
      p={4}
    >
      {itineraries.map((itinerary) => (
        <ItineraryCard
          key={itinerary.id}
          itinerary={itinerary}
        />
      ))}
    </SimpleGrid>
    }
  </ChakraProvider>
};
