import fetchMock from 'jest-fetch-mock';
import { screen, within } from '@testing-library/react'
import { render } from './test-utils';
import { App } from './App';
import { SAMPLE_ITINERARIES } from './utils/api';
import { act } from 'react-dom/test-utils';

// https://github.com/enzymejs/enzyme/issues/2073
const waitForComponentToPaint = async () => {
  await act(async () => {
    await new Promise(resolve => setTimeout(resolve, 0));
  });
};

describe('App render state', () => {
  beforeEach(() => {
    fetchMock.enableMocks();
    fetchMock.resetMocks();
  });
  
  test('renders initial loading', async () => {
    render(<App />);
  
    const spinnerEl = await screen.findByTestId('loading-spinner');
    expect(spinnerEl).toBeInTheDocument();
  
    await waitForComponentToPaint();
  });
  
  test('renders itinerary grid when itineraries are present', async () => {
    fetchMock.mockResponse(JSON.stringify(SAMPLE_ITINERARIES));
    render(<App />);
  
    const itineraryGridEl = await screen.findByTestId('itinerary-grid');
    for (const itinerary of SAMPLE_ITINERARIES) {
      expect(await within(itineraryGridEl).findByText(itinerary.title)).toBeInTheDocument();
    }
  
    await waitForComponentToPaint();
  });
  
  test('renders message when itineraries are not present', async () => {
    fetchMock.mockResponse(JSON.stringify([]));
    render(<App />);
  
    const alertMessageEl = await screen.findByRole('alert');
    expect(await within(alertMessageEl).findByText('No data to display')).toBeInTheDocument();
    
    await waitForComponentToPaint();
  });
  
  test('renders error message when api call fails', async () => {
    fetchMock.mockReject(new Error());
    render(<App />);
  
    const alertMessageEl = await screen.findByRole('alert');
    expect(await within(alertMessageEl).findByText('An error has occurred')).toBeInTheDocument();
  
    await waitForComponentToPaint();
  });  
});
