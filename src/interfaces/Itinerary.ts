export default interface Itinerary {
    id: number,
    title: string,
    rating: number,
    countryCount: number,
    dayDuration: number,
    emissionKgs: number,
    imgSrc: string
};
