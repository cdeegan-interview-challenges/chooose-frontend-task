import { Box, Card, Flex, Heading, Spacer, Text } from '@chakra-ui/react';
import StarRatings from 'react-star-ratings';
import pluralize from 'pluralize';
import Itinerary from '../interfaces/Itinerary';
import { formatEmissionsWithUnit } from '../utils/format';

interface ItineraryCardProps {
  itinerary: Itinerary;
};

export const ItineraryCard: React.FC<ItineraryCardProps> = ({ itinerary }: ItineraryCardProps) => {
  return <Card borderRadius={20}>
    <Flex
      direction='column'
      px={8}
      gap='4'
      borderRadius={20}
      style={{
        border: '8px solid white',
        backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0)), url(${itinerary.imgSrc})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center'
      }}
    >
      <Box height={6}></Box>
      <Box>
        <Flex direction='column' gap={4}>
          <Box>
            <Heading fontSize='xl' color='white' textAlign='center'>{itinerary.title}</Heading>
            <Text fontSize='sm' color='white' align='center'>{itinerary.countryCount} {pluralize('country', itinerary.countryCount)}, {itinerary.dayDuration} {pluralize('day', itinerary.dayDuration)}</Text>
          </Box>
          <Box bg='#1F2837' p={4} borderRadius={12}>
            <Flex>
              <Text color='white' align='center'>Emissions offset:</Text>
              <Spacer></Spacer>
              <Text color='white' align='center'>{formatEmissionsWithUnit(itinerary.emissionKgs)} CO<sub>2</sub>e</Text>
            </Flex>
          </Box>
        </Flex>
      </Box>
      <Box bg='white' p={4} borderTopRadius={12} textAlign='center'>
        <Flex>
          <Text>Trip Rating</Text>
          <Spacer />
          <Box mr={4}>
            <StarRatings
              rating={itinerary.rating}
              starDimension='16px'
              starSpacing='2px'
              starRatedColor='#FFCC00'
            />
          </Box>
          <Text fontSize='lg' fontWeight='bold'>{itinerary.rating}</Text>
        </Flex>
      </Box>
    </Flex>
  </Card>
};
