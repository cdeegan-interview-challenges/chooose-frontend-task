import { extendTheme } from '@chakra-ui/react';
import '@fontsource/signika-negative';
import '@fontsource/varela-round';

const theme = extendTheme({
  fonts: {
    heading: `'Signika Negative', sans-serif`,
    body: `'Varela Round', sans-serif`,
  },
  styles: {
    global: () => ({
      body: {
        bg: '#F6F7F9',
      }
    })
  }
});

export default theme;